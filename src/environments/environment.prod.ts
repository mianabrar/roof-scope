export const environment = {
  production: true,
  version: require('../../package.json').version,
  apiEndPoint: 'https://qa-tracescope.myscopetech.com/',
};
