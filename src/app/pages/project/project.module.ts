import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TabsModule } from "ngx-bootstrap/tabs";
import { CollapseModule } from "ngx-bootstrap/collapse";
import { NgxNavbarModule } from "ngx-bootstrap-navbar";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

import { MenuModule } from "./menu/menu.module";
import { FooterModule } from "./footer/footer.module";
import { ProjectRoutingModule } from "./project-routing.module";
import { EngineComponentDirective } from "src/app/engine/engine-component.directive";
import { EngineComponent } from "src/app/engine/engine.component";
import { ProjectComponentDirective } from "./project-component.directive";
import { ProjectComponent } from "./project.component";
import { ProjectInformationComponent } from "./project-information/project-information.component";
import { ImageAcquisitionComponentDirective } from "./image-acquisition/image-acquisition-component.directive";
import { ImageAcquisitionComponent } from "./image-acquisition/image-acquisition.component";
import { FourImageComponent } from "./image-acquisition/four-image/four-image.component";
import { TwoImagesComponent } from "./image-acquisition/two-images/two-images.component";
import { OneImageComponent } from "./image-acquisition/one-image/one-image.component";
import { MeasureAlignComponent } from "./measure-align/measure-align.component";
import { LineEditorComponent } from "./line-editor/line-editor.component";
import { AreaEditorComponent } from "./area-editor/area-editor.component";
import { ReportsComponent } from "./reports/reports.component";
import { FormsModule } from "@angular/forms";
import { TabMenuSelectionComponent } from "./tab-menu-selection/tab-menu-selection.component";
import { AccordionModule } from "ngx-bootstrap/accordion";

import { BsDropdownModule,BsDropdownConfig } from 'ngx-bootstrap/dropdown';
import { SubmitErrorComponent } from './dialog/submit-error/submit-error.component';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { NgxFileDropModule } from 'ngx-file-drop';
import { CloseProjectComponent } from './dialog/close-project/close-project.component';

@NgModule({
  declarations: [
    EngineComponentDirective,
    EngineComponent,
    ProjectComponentDirective,
    ProjectComponent,
    ProjectInformationComponent,
    ImageAcquisitionComponentDirective,
    ImageAcquisitionComponent,
    FourImageComponent,
    TwoImagesComponent,
    OneImageComponent,
    MeasureAlignComponent,
    LineEditorComponent,
    AreaEditorComponent,
    ReportsComponent,
    TabMenuSelectionComponent,
    SubmitErrorComponent,
    CloseProjectComponent,
  ],
  imports: [
    CommonModule,

    TabsModule.forRoot(),
    CollapseModule.forRoot(),
    NgxNavbarModule,
    FontAwesomeModule,
    BsDropdownModule.forRoot(),
    MenuModule,
    FooterModule,
    ProjectRoutingModule,
    FormsModule,
    AccordionModule.forRoot(),
    PopoverModule.forRoot(),
    NgxFileDropModule
  ],
  entryComponents: [
    EngineComponent,
    ProjectInformationComponent,
    ImageAcquisitionComponent,
    FourImageComponent,
    MeasureAlignComponent,
    OneImageComponent,
    LineEditorComponent,
    AreaEditorComponent,
    ReportsComponent,
  ],
  providers: [BsDropdownConfig]
})
export class ProjectModule {}
