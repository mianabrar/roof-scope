import { Component, ViewEncapsulation } from "@angular/core";
import { Subscription } from "rxjs";
import { MessageService } from "src/app/services/message.service";
import { EngineService } from "src/app/engine/engine.service";

@Component({
  selector: "app-menu-align-scale",
  templateUrl: "./menu-align-scale.component.html",
  styleUrls: ["./menu-align-scale.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class MenuAlignScaleComponent {
  subscription: Subscription;

  align_btn_enabled:boolean;
  measure_btn_enabled:boolean;

  constructor(private engServ: EngineService, private MS: MessageService) {
    // subscribe to component messages
    this.subscription = this.MS.getMessage().subscribe((message) => {
      if (message == "DISABLE_ALL_MENU_BUTTONS") {
        this.falseAll();
      }
    });
    this.align_btn_enabled = this.MS.measure_align.ALIGN;
    this.measure_btn_enabled = this.MS.measure_align.MEASURE;
  }

  align_clickHandler() {
    this.MS.sendMessage("DISABLE_ALL_MENU_BUTTONS");
    this.MS.measure_align.ALIGN = this.align_btn_enabled = true;

    document.body.style.cursor = "default";
    this.engServ.selectionHelper.element.className = "selectionBoxHide";
  }

  measure_clickHandler() {
    this.MS.sendMessage("DISABLE_ALL_MENU_BUTTONS");
    this.MS.measure_align.MEASURE = this.measure_btn_enabled = true;

    document.body.style.cursor = "default";
    this.engServ.selectionHelper.element.className = "selectionBoxHide";
  }

  falseAll() {
    this.align_btn_enabled = this.MS.measure_align.ALIGN = false;
    this.measure_btn_enabled = this.MS.measure_align.MEASURE = false;
  }
}
