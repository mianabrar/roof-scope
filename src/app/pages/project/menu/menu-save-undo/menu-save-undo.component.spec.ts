import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuSaveUndoComponent } from './menu-save-undo.component';

describe('MenuSaveUndoComponent', () => {
  let component: MenuSaveUndoComponent;
  let fixture: ComponentFixture<MenuSaveUndoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuSaveUndoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuSaveUndoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
