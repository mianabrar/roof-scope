//https://www.npmjs.com/package/ngx-countdown
import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { Subscription } from 'rxjs';
import { CountdownComponent, CountdownConfig, CountdownEvent } from 'ngx-countdown';

@Component({
  selector: 'app-menu-timer',
  templateUrl: './menu-timer.component.html',
  styleUrls: ['./menu-timer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuTimerComponent {

  subscription: Subscription;

  CountdownTimeUnits: Array<[string, number]> = [
    ['Y', 1000 * 60 * 60 * 24 * 365], // years
    ['M', 1000 * 60 * 60 * 24 * 30], // months
    ['D', 1000 * 60 * 60 * 24], // days
    ['H', 1000 * 60 * 60], // hours
    ['m', 1000 * 60], // minutes
    ['s', 1000], // seconds
    ['S', 1], // million seconds
  ];

  moreThan24Hours: CountdownConfig = {
    leftTime: 0,
    demand: true,
    formatDate: ({ date, formatStr }) => {
      let duration = Number(date || 0);

      return this.CountdownTimeUnits.reduce((current, [name, unit]) => {
        if (current.indexOf(name) !== -1) {
          const v = Math.floor(duration / unit);
          duration -= v * unit;
          return current.replace(new RegExp(`${name}+`, 'g'), (match: string) => {
            return v.toString().padStart(match.length, '0');
          });
        }
        return current;
      }, formatStr);
    },
  };

  @ViewChild('countdown', { static: false }) private counter: CountdownComponent;

  constructor(private MS: MessageService) {

    // subscribe to component messages
    this.subscription = this.MS.getMessage().subscribe((message) => {

      if (message == "START_TIMER") {
        this.startTimer();
      }

    });

  }

  startTimer() {

    if( this.MS.project_information.DUE_DATE == null) {
      this.moreThan24Hours.leftTime = 0;
    }
    else {
      let nowDate:Date = new Date();
      let dueDate:Date = new Date( this.MS.project_information.DUE_DATE );
      let diff:any = dueDate.getTime() - nowDate.getTime();
      let seconds_from_nowDate_to_dueDate = diff/1000;
      let seconds_between_dates = Math.abs(seconds_from_nowDate_to_dueDate);

      this.moreThan24Hours = { leftTime: seconds_between_dates };
      this.counter.restart();

    }

  }



}
