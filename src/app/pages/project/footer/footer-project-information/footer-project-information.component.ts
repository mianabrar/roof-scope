import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-footer-project-information',
  templateUrl: './footer-project-information.component.html',
  styleUrls: ['./footer-project-information.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class FooterProjectInformationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
