import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterReportsComponent } from './footer-reports.component';

describe('FooterReportsComponent', () => {
  let component: FooterReportsComponent;
  let fixture: ComponentFixture<FooterReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
