import { Component, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { MessageService } from 'src/app/services/message.service';
import { ScdFileService } from 'src/app/services/scdfile.service';

@Component({
  selector: 'app-footer-line-editor',
  templateUrl: './footer-line-editor.component.html',
  styleUrls: ['./footer-line-editor.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FooterLineEditorComponent {

  objectSnaps:boolean = false;
  radial:boolean = false;
  endPoint:boolean = false;
  midPoint:boolean = false;
  intersection:boolean = false;
  perpendicular = false;
  nearest:boolean = false;
  grid:boolean = false;

  constructor(public MS: MessageService, private gService: ScdFileService) { 
    this.objectSnaps = this.MS.settings.OBJECT_SNAPS;
  }

  gridSnaps_clickHandler() {

  }

  radial_clickHandler() {
    this.MS.footer_lineEditor.RADIAL = this.radial;
  }

  endPoint_clickHandler() {
    this.MS.footer_lineEditor.END_POINT = this.endPoint;
  }

  midPoint_clickHandler() {
    this.MS.footer_lineEditor.MID_POINT = this.midPoint;
  }

  intersection_clickHandler() {
    this.MS.footer_lineEditor.INTERSECTION = this.intersection;
  }

  perpendicular_clickHandler() {
    this.MS.footer_lineEditor.PERPENDICULAR = this.perpendicular;
  }

  nearest_clickHandler() {
    this.MS.footer_lineEditor.NEAREST = this.nearest;
  }

  grid_clickHandler() {
    this.MS.footer_lineEditor.GRID = this.grid;
  }

  objectSnaps_clickHandler(event) {
    this.MS.settings.OBJECT_SNAPS = this.objectSnaps;
    this.gService.saveUserSettings(this.MS.settings);

  }

}
