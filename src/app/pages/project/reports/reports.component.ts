import { Component } from "@angular/core";
import { Subscription } from "rxjs";
import { MessageService } from "src/app/services/message.service";
import { EngineService } from 'src/app/engine/engine.service';
import {
  trigger,
  transition,
  state,
  animate,
  style,
} from "@angular/animations";
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: "app-reports",
  templateUrl: "./reports.component.html",
  styleUrls: ["./reports.component.scss"],
  animations: [
    trigger("flyInOut", [
      state("in", style({ transform: "translateX(0)" })),
      transition("void => *", [
        style({ transform: "translateX(-100%)" }),
        animate(500),
      ]),
      transition("* => void", [
        animate(500, style({ transform: "translateX(100%)" })),
      ]),
    ]),
  ],
})
export class ReportsComponent {
  subscription: Subscription;
  polygonData = new Array();
  buildingData : any = this.MS.project_information.BUILDINGS;


  constructor(private MS: MessageService,  public engServ: EngineService, private router: Router, private toastr: ToastrService) {
    this.subscription = this.MS.getMessage().subscribe((message) => {
      if( message == "PROJECT_REPORTS_INIT") {
        this.init();
        
      }
      else if( message == "GENERATE_REPORT_PDF") {
        this.showPDF();
        
      }
    });
  }

  private init() {
    this.showScreenshot();
    this.showReport();
  }
  
  private showScreenshot() {
    let img = this.MS.area_editor.SCREENSHOT;
    
    if( img != null ) {
      document.getElementById("screenshot").innerHTML = "<img src='" + img.src + "' width='100%'  >"
    }

  }
  
  private showReport() {
    this.polygonData = this.MS.area_editor.POLYGON_DATA;
    console.log("Polygon Data > ", this.polygonData);
    
    
    
  }

  private showPDF() {
    var element = document.getElementById('report-pdf');
    var opt = {
      margin:       1,
      filename:     'report.pdf',
      image:        { type: 'jpeg', quality: 0.98 },
      html2canvas:  { scale: 2 },
      jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
    };
    //Promise-based usage:
    html2pdf().from(element).set(opt).save();
    let scope = this;
    if ( this.MS.saveAndSubmit.isSaveAndSubmit == true ) {
      html2pdf().from(element).set(opt).toPdf().output('datauristring').then(function (pdfAsString) {
        scope.MS.report.PDF = pdfAsString.split(",")[1];
        scope.toastr.success('Project has been closed and submitted!', "Success!");
        scope.router.navigate["open-project"];
      });
    }
  }

  

}
