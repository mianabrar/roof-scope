import { Vector3 } from "three";
import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { EngineService } from './engine.service';
import { MessageService } from './../services/message.service';
import { FunctionService } from '../services/function.service';
import { HotkeyService } from '../services/hotkey.service';

import { Align } from './tasks/align';
import { Measure } from './tasks/measure';
import { CanvasImage } from './tasks/canvas-image';
import { OrbitControlsState } from "./tasks/orbit-controls-state";

import { DimensionsShape } from './shapes/dimensions-shape';
import { LineShape } from './shapes/line-shape';
import { RectangleShape } from './shapes/rectangle-shape';
import { CircleShape } from './shapes/circle-shape';
import { EllipseShape } from './shapes/ellipse-shape';
import { ArcShape } from './shapes/arc-shape';
import { TextShape } from './shapes/text-shape';

import { ModifyMove } from './tasks/modify-move/modify-move';
import { ModifyStretch } from './tasks/modify-stretch/modify-stretch';
import { ModifyRotate } from './tasks/modify-rotate/modify-rotate';
import { ModifyOffset } from './tasks/modify-offset/modify-offset';
import { ModifyMirror } from './tasks/modify-mirror/modify-mirror';
import { ModifyExtend } from './tasks/modify-extend/modify-extend';
import { ModifyFillet } from './tasks/modify-fillet/modify-fillet';
import { ModifyTrim } from './tasks/modify-trim/modify-trim';
import { ModifyDimensions } from './tasks/modify-dimensions/modify-dimensions';

import { Selection } from './tasks/selection';

import { Area } from "./tasks/area";
import { PolygonDetect } from './tasks/polygon-detect';
import { Report } from './tasks/report';

import { SaveSCD } from "../tasks/saveSCD/save-scd";
import { LoadSCD } from "../tasks/loadSCD/load-scd";
import { BsModalService } from 'ngx-bootstrap/modal';
import { Clipboard } from './tasks/clipboard';
import { Crosshair } from './shapes/crosshair';
import { Submit } from '../tasks/submit';


@Component({
  selector: "app-engine",
  templateUrl: "./engine.component.html",
  styleUrls: ["./engine.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class EngineComponent implements OnInit {
  @ViewChild("rendererCanvas", { static: true })
  public rendererCanvas: ElementRef<HTMLCanvasElement>;

  componentID: number;
  startPositionMouse = { x: 0, y: 0 };
  startPositionTouch = { x: 0, y: 0 };
  align: Align;
  measure: Measure;
  canvasImage: CanvasImage;
  orbitControlState: OrbitControlsState;
  
  dimensionsShape: DimensionsShape;
  lineShape: LineShape;
  rectangleShape:RectangleShape;

  circleShape:CircleShape;
  ellipseShape:EllipseShape;
  arcShape:ArcShape;
  textShape:TextShape;

  modifyMove: ModifyMove;
  modifyStretch: ModifyStretch;
  modifyRotate: ModifyRotate;
  modifyOffset: ModifyOffset;
  modifyMirror: ModifyMirror;
  modifyExtend: ModifyExtend;
  modifyFillet: ModifyFillet;
  modifyTrim: ModifyTrim;
  modifyDimensions: ModifyDimensions;

  selection: Selection;
  
  area: Area; 
  polygonDetect:PolygonDetect; 
  report: Report;

  saveSCD: SaveSCD;
  loadSCD: LoadSCD;
  submit: Submit;

  clipboard: Clipboard;
  crosshair:Crosshair;

  subscription: Subscription;

  constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService, private modalService: BsModalService, private changeDetection: ChangeDetectorRef, private hotKey: HotkeyService ) { 
    
    // subscribe to component messages
    this.subscription = this.MS.getMessage().subscribe(message => {

      if (message) {
        const msg:string = message.split(':')[0];   

        //This will execute only when Canvas page is opened everytime
        if( msg == "ComponentForEngine" ) {
          const componentID:number = message.split(':')[1];
          this.componentID = componentID;

          let areasGroup:any = this.engServ.getSelectedAreasGroup();

          //if Project Information or Imagery page is in view, do nothing
          if( componentID < 2 ) 
            return;

          //save and reset zoom for measure & align, and line editor components seperately
          this.orbitControlState.change(componentID);

          //-----------------------------------------------------------Measue and Align page is opened
          if( componentID == 2 ) {
            this.canvasImage.loadImage();
            this.engServ.hideAllBuildingsGroups();
            this.MS.sendMessage('REMOVE_ALL_DIMENSIONS');
            this.MS.sendMessage('HIDE_ALL_POLYGONS');
            
            this.align.show();
            this.measure.show();
          }
          //-----------------------------------------------------------Line Editor page is opened
          else if( componentID == 3 ) {
            this.canvasImage.loadImage();
            this.engServ.showSelectedBuildingsGroup( this.MS.menu_line_editor.BUILDING_GROUP_SELECTED );
            this.MS.sendMessage('ADD_ALL_DIMENSIONS');
            this.MS.sendMessage('HIDE_ALL_POLYGONS');
            this.align.hide();
            this.measure.hide();
          }
          //-----------------------------------------------------------Area Editor page is opened
          else if( componentID == 4 ) {
            
            this.canvasImage.loadImage();
            this.engServ.showSelectedBuildingsGroup( this.MS.menu_line_editor.BUILDING_GROUP_SELECTED );
            this.MS.sendMessage('ADD_ALL_DIMENSIONS');
            this.MS.sendMessage('POLYGON_DETECT_INIT');
            this.MS.sendMessage("SHOW_POLYGON_SVG");
            
            this.align.hide();
            this.measure.hide();
           }
          //-----------------------------------------------------------Report page is opened
          else if (componentID == 5) {
            this.MS.sendMessage("CHECK_PLANES");
            this.report.init();
          }

        }
      }
    });
  }

  ngOnInit() {

    this.engServ.clearSession();
    this.engServ.createScene(this.rendererCanvas);

    this.engServ.animate();

    this.align = new Align(this.engServ, this.MS, this.FS);
    this.measure = new Measure(this.engServ, this.MS, this.FS);
    this.canvasImage = new CanvasImage(this.engServ, this.MS);
    this.orbitControlState = new OrbitControlsState(this.engServ);

    this.dimensionsShape = new DimensionsShape(this.engServ, this.MS, this.FS);
    this.lineShape = new LineShape(this.engServ, this.MS, this.FS, this.dimensionsShape);
    this.rectangleShape = new RectangleShape(this.engServ, this.MS, this.FS, this.dimensionsShape);

    this.circleShape = new CircleShape(this.engServ, this.MS, this.FS, this.dimensionsShape);
    this.ellipseShape = new EllipseShape(this.engServ, this.MS, this.FS, this.dimensionsShape);
    this.arcShape = new ArcShape(this.engServ, this.MS, this.FS, this.dimensionsShape);  
    this.textShape = new TextShape(this.engServ, this.MS, this.FS);   

    this.area = new Area(this.engServ, this.MS, this.FS); 
    this.polygonDetect = new PolygonDetect(this.engServ,this.MS,this.FS);
    this.report = new Report(this.engServ, this.MS, this.FS);

    this.selection = new Selection(this.engServ, this.MS, this.FS );

    this.modifyMove = new ModifyMove(this.engServ, this.MS, this.FS, this.dimensionsShape, this.selection);
    this.modifyStretch = new ModifyStretch(this.engServ, this.MS, this.FS, this.dimensionsShape, this.selection);
    this.modifyRotate = new ModifyRotate(this.engServ, this.MS, this.FS, this.dimensionsShape, this.selection);
    this.modifyOffset = new ModifyOffset(this.engServ, this.MS, this.FS, this.dimensionsShape, this.selection);
    this.modifyMirror = new ModifyMirror(this.engServ, this.MS, this.FS, this.dimensionsShape, this.selection);
    this.modifyExtend = new ModifyExtend(this.engServ, this.MS, this.FS, this.dimensionsShape, this.selection);
    this.modifyFillet = new ModifyFillet(this.engServ, this.MS, this.FS, this.dimensionsShape, this.selection);
    this.modifyTrim = new ModifyTrim(this.engServ, this.MS, this.FS, this.dimensionsShape, this.selection);
    this.modifyDimensions = new ModifyDimensions(this.engServ, this.MS, this.FS);
        
    this.saveSCD = new SaveSCD(this.engServ, this.MS);
    this.loadSCD = new LoadSCD(this.engServ, this.MS, this.measure, this.align);
    this.submit = new Submit(this.engServ, this.MS,  this.modalService)

    this.clipboard = new Clipboard(this.engServ, this.MS, this.FS, this.dimensionsShape, this.selection);
    this.crosshair = new Crosshair(this.engServ, this.MS);
  }

  dblclick(e:MouseEvent) {
    console.log('dblclick')
  }

  keydown(e:KeyboardEvent) { 
    //-----------------------------------------------------------Line Editor
    if( this.componentID == 3 ) {

      this.stopSelectionModify();    
      this.hotKey.keyDownLineEditor(e); 	

    }
    //-----------------------------------------------------------Area Editor
    else if( this.componentID == 4 ) {
        this.area.onKeyDown(e);
    }
  }

  keyup(e:KeyboardEvent) { 
    //-----------------------------------------------------------Area Editor
    if( this.componentID == 4 ) {
      this.area.onKeyUp(e);
    }
  }

  mouseup(e:MouseEvent) { 

    //if left mouse button clicked
    if( e.which == 1 ) {

      if( this.MS.menu_zoom.WINDOW ) {
        this.MS.sendMessage('MENU_ZOOM_WINDOW_ZOOM_TO_SELECTION');
      }
      
      //-----------------------------------------------------------Line Editor
      else if (this.componentID == 3) {

        if( this.MS.menu_modify.VIEW_PORT ) {
          this.MS.sendMessage('MENU_MODIFY_VIEWPORT');
        }

      }

      //-----------------------------------------------------------Area Editor
      else if (this.componentID == 4) {

        if(this.MS.area_editor.DRAGGING_POLYGON) {
          this.MS.area_editor.DRAGGING_POLYGON = false;
        }
          
      }
      

    }

    
  }
 
  click(e:MouseEvent) {
    // Cache the client X/Y coordinates
    this.startPositionMouse.x = e.pageX;
    this.startPositionMouse.y = e.pageY;
    const relative:Vector3 = this.engServ.get3DPosition( e.clientX, e.clientY );

    if(relative==null) return;

    //left mouse click
    if(e.which == 1) { 
      this.click_touchstart(relative, e);      
    }
    
  }

  mousedown(e:MouseEvent) {
    // Cache the client X/Y coordinates
    this.startPositionMouse.x = e.pageX;
    this.startPositionMouse.y = e.pageY;
    const relative:Vector3 = this.engServ.get3DPosition( e.clientX, e.clientY );

    if(relative==null) return;

    //left mouse click
    if(e.which == 1) { 
      this.mousedown_touchstart(relative, e);      
    }
    //middle mouse click
    if(e.which == 2) { 
      this.engServ.selectionHelper.element.className = "selectionBoxHide";
    }
    //right mouse click
    if(e.which == 3) {
      this.onStop(relative);
    }
    
  }
  
  mouseout (e: MouseEvent) {
    this.MS.sendMessage("CROSSHAIR_HIDE");
  }

  mouseenter (e: MouseEvent) {
    this.MS.sendMessage("CROSSHAIR_SHOW");
  }

  touchstart(e:TouchEvent) { 
    // Cache the client X/Y coordinates
    this.startPositionTouch.x = e.touches[0].clientX;
    this.startPositionTouch.y = e.touches[0].clientY;
    const relative:Vector3 = this.engServ.get3DPosition( e.touches[0].clientX, e.touches[0].clientY );
    
    this.click_touchstart(relative, e);  
  }

  touchend(e:TouchEvent) {
    // Compute the change in X and Y coordinates. 
    // The first touch point in the changedTouches
    // list is the touch point that was just removed from the surface.
    let clientX:number = (e.changedTouches[0].clientX - this.startPositionTouch.x) ;
    let clientY:number = (e.changedTouches[0].clientY - this.startPositionTouch.y) ;
    const relative:Vector3 = this.engServ.get3DPosition(clientX, clientY);

    this.click_touchstart(relative, e);  
  }

  click_touchstart(relative:Vector3, e:any) {
    //-----------------------------------------------------------Measure and Align
    if( this.componentID == 2 ) {

      //align
      if(this.MS.measure_align.ALIGN)
        this.align.onMouseDown(relative);
      //measure
      else if(this.MS.measure_align.MEASURE)
        this.measure.onMouseDown(relative);

    }  
    //-----------------------------------------------------------Line Editor
    else if( this.componentID == 3 ) {

      //if object is clicked in selection class
      let isSelectionClick:boolean = false;
      if(this.selection.enabled)
        isSelectionClick = this.selection.onClick(e);  

      //-------------drawing
      //draw line
      if(this.MS.menu_insert.LINE)
        this.lineShape.onClick(relative);
      //draw rectangle
      else if(this.MS.menu_insert.RECTANGLE)
        this.rectangleShape.onClick(relative);  
      //draw circle
      else if(this.MS.menu_insert.CIRCLE)
        this.circleShape.onClick(relative);    
      //draw ellipse
      else if(this.MS.menu_insert.ELLIPSE)
        this.ellipseShape.onClick(relative);    
      //draw arc
      else if(this.MS.menu_insert.ARC)
        this.arcShape.onClick(relative);  
      //draw text
      else if(this.MS.menu_insert.TEXT)
        this.textShape.onClick(relative);  

        //-------------modifyng
      //modify move  
      else if( this.MS.menu_modify.MOVE ) {
        if( !isSelectionClick ) this.modifyMove.onClick(relative);
      }
      //modify rotate  
      else if( this.MS.menu_modify.ROTATE ) {
        if( !isSelectionClick ) this.modifyRotate.onClick(relative);  
      }
      //modify offset  
      else if( this.MS.menu_modify.OFFSET ) {
        if( !isSelectionClick ) this.modifyOffset.onClick(relative);    
      }
      //modify mirror  
      else if( this.MS.menu_modify.MIRROR ) {
        if( !isSelectionClick ) this.modifyMirror.onClick(relative);     
      } 
      //modify extend  
      else if( this.MS.menu_modify.EXTEND ) 
        this.modifyExtend.onClick(relative);   
      //modify fillet  
      else if( this.MS.menu_modify.FILLET ) 
        this.modifyFillet.onClick(relative);    
      //modify trim  
      else if( this.MS.menu_modify.TRIM ) 
        this.modifyTrim.onClick(relative);    
         //modify stretch  
      else if( this.MS.menu_modify.STRETCH ) 
        this.modifyStretch.onClick(relative);
        
      //clipboard
      if(this.clipboard.enabled) {
        this.clipboard.onClick(e);
      }  
   
      //modify dimensions
      if( this.MS.menu_line_editor.EDIT_DIMS )
        this.modifyDimensions.onClick(relative);  


    }

    //-----------------------------------------------------------Area Editor
    else if( this.componentID == 4 ) {
      this.area.onClick(relative);     
    }


  }


  mousedown_touchstart(relative:Vector3, e:any) {
    //-----------------------------------------------------------Line Editor
    if( this.componentID == 3 ) {

      
      //modify move  
      if( this.MS.menu_modify.MOVE ) 
        this.modifyMove.onMouseDown(relative);
      //modify stretch  
      else if( this.MS.menu_modify.STRETCH ) 
        this.modifyStretch.onMouseDown(relative);
      //modify rotate  
      else if( this.MS.menu_modify.ROTATE ) 
        this.modifyRotate.onMouseDown(relative);  
      //modify offset  
      else if( this.MS.menu_modify.OFFSET ) 
        this.modifyOffset.onMouseDown(relative);    
      //modify mirror  
      else if( this.MS.menu_modify.MIRROR ) 
        this.modifyMirror.onMouseDown(relative);     
    
      //modify trim  
      else if( this.MS.menu_modify.TRIM ) 
        this.modifyTrim.onMouseDown(relative);    

      if(this.selection.enabled)
        this.selection.onMouseDown(e);
    }
    //-----------------------------------------------------------Area Editor
    if( this.componentID == 4 ) {
      this.area.onMouseDown(relative);     
    }
  }


  onStop(relative: Vector3) {

    //-----------------------------------------------------------Line Editor
    if (this.componentID == 3) {


      //draw line
      if(this.MS.menu_insert.LINE)
        this.lineShape.onStop(relative);      
      //draw rectangle
      else if (this.MS.menu_insert.RECTANGLE)
        this.rectangleShape.onStop(relative);

      //draw circle
      else if(this.MS.menu_insert.CIRCLE)
        this.circleShape.onStop(relative); 
      //draw ellipse
      else if(this.MS.menu_insert.ELLIPSE)
        this.ellipseShape.onStop(relative);  
      //draw arc
      else if(this.MS.menu_insert.ARC)
        this.arcShape.onStop(relative);  
   
        
      this.stopSelectionModify();  

            
    }  

  }

  mousemove(e:MouseEvent) { 
    let relative:Vector3;
    if(e.button == 0) {
      relative = this.engServ.get3DPosition( e.clientX, e.clientY );

      this.mousemove_touchmove(relative, e);
    }

  }

  touchmove(e: TouchEvent) {
    const relative: Vector3 = this.engServ.get3DPosition(
      e.touches[0].clientX,
      e.touches[0].clientY
    );

    this.mousemove_touchmove(relative, e);
  }


  mousemove_touchmove(relative:Vector3, e:any) {
    //-----------------------------------------------------------Measure and Align
    if( this.componentID == 2 ) {

      //crosshair
      this.crosshair.onMouseMove(relative);

      //align
      if(this.MS.measure_align.ALIGN)
        this.align.onMouseMove(relative);

      //measure
      else if(this.MS.measure_align.MEASURE)
        this.measure.onMouseMove(relative);
        

    } 
    //-----------------------------------------------------------Line Editor
    else if( this.componentID == 3 ) {

      //crosshair
      this.crosshair.onMouseMove(relative);

      //clipboard
      if(this.clipboard.enabled) 
        this.clipboard.onMouseMove(relative);

      if(this.selection.enabled)
        this.selection.onMouseMove(e);

      //modify dimensions
      if( this.MS.menu_line_editor.EDIT_DIMS )
        this.modifyDimensions.onMouseMove(relative, e);  

      //draw line
      else if( this.MS.menu_insert.LINE )
        this.lineShape.onMouseMove(relative, e);
      //draw rectangle
      else if(this.MS.menu_insert.RECTANGLE)
        this.rectangleShape.onMouseMove(relative, e);  
 
      //draw circle
      else if(this.MS.menu_insert.CIRCLE)
        this.circleShape.onMouseMove(relative, e);   
      //draw ellipse
      else if(this.MS.menu_insert.ELLIPSE)
        this.ellipseShape.onMouseMove(relative, e);   
      //draw arc
      else if(this.MS.menu_insert.ARC)
        this.arcShape.onMouseMove(relative, e); 
      //draw text
      else if(this.MS.menu_insert.TEXT)
        this.textShape.onMouseMove(relative, e);        
  
      //modify move  
      else if( this.MS.menu_modify.MOVE ) 
        this.modifyMove.onMouseMove(relative, e);
      //modify stretch  
      else if( this.MS.menu_modify.STRETCH ) 
        this.modifyStretch.onMouseMove(relative, e);  
      //modify rotate  
      else if( this.MS.menu_modify.ROTATE ) 
        this.modifyRotate.onMouseMove(relative, e);    
      //modify offset  
      else if( this.MS.menu_modify.OFFSET ) 
        this.modifyOffset.onMouseMove(relative, e);
      //modify mirror  
      else if( this.MS.menu_modify.MIRROR ) 
        this.modifyMirror.onMouseMove(relative, e);
      //modify extend  
      else if( this.MS.menu_modify.EXTEND ) 
        this.modifyExtend.onMouseMove(relative, e);  
      //modify fillet  
      else if( this.MS.menu_modify.FILLET ) 
        this.modifyFillet.onMouseMove(relative, e);
      //modify trim  
      else if( this.MS.menu_modify.TRIM ) 
        this.modifyTrim.onMouseMove(relative, e);      
      
    }  

    //-----------------------------------------------------------Area Editor
    else if( this.componentID == 4 ) {
      this.area.onMouseMove(relative, e);
    }


  }
  
  stopSelectionModify() {

    //clipboard
    if(this.clipboard.enabled) {
      this.clipboard.onStop();
    }

    //modify move  
    if( this.MS.menu_modify.MOVE ) 
    this.modifyMove.onStop();
    //modify stretch  
    else if( this.MS.menu_modify.STRETCH ) 
      this.modifyStretch.onStop();
    //modify rotate  
    else if( this.MS.menu_modify.ROTATE ) 
      this.modifyRotate.onStop();  
    //modify offset  
    else if( this.MS.menu_modify.OFFSET ) 
      this.modifyOffset.onStop();   
    //modify mirror  
    else if( this.MS.menu_modify.MIRROR ) 
      this.modifyMirror.onStop();  
     
    //modify fillet  
    else if( this.MS.menu_modify.FILLET ) 
      this.modifyFillet.onStop();   

      
    if(this.selection.enabled)
      this.selection.onStop();  

  }

}
