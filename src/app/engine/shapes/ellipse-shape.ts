import { EllipseCurve, LineDashedMaterial, BufferGeometry, Vector3, Line, BufferAttribute } from 'three';
import { EngineService } from '../engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from './../../services/function.service';
import { Subscription } from 'rxjs';
import { DimensionsShape } from './dimensions-shape';

export class EllipseShape {

    private ellipse:any;
    private ellipsePosition:Vector3;
    private count:number = 0;
    private MAX_POINTS:number = 2;
    private centerPosition:Vector3;
    private gridSnapPoint:Vector3 = null;
    private xRadius:number;
    private aRotation:number = 0;
    private line1:any;
    private line2:any;
    private positions1:any;
    private positions2:any;

    subscription: Subscription;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService, private dimensionsShape:DimensionsShape ) {
    
        // subscribe to component messages
        this.subscription = this.MS.getMessage().subscribe((message) => {
            if (message == "MENU_LINE_TYPE_CHANGED" || message == "MENU_PITCH_CHANGED") { 

                //if circle is already being drawn
                if( this.count > 0 ) {
                    this.engServ.update_line_shape( this.ellipse );
                }

            }
        })
    
    }

    onClick( relative:Vector3 ) {

        // on first click add a point
        if( this.count === 0 ){
            this.ellipsePosition = relative;
            let geometry:BufferGeometry = this.ellipseGeometry( 0.1, 0.1 );

            this.ellipse = this.engServ.set_line_shape(geometry);

            this.ellipse.name = 'Ellipse';
            
            this.engServ.addToScene(this.ellipse);

            let material = new LineDashedMaterial({ 
                color: this.MS.color.RADIUS_LINE, 
                dashSize: 1,
                gapSize: 1,
            });

            geometry = this.lineGeometry1();
            this.line1 = new Line(geometry, material);
            this.line1.name = 'EllipseRadius1';
            this.ellipse.add( this.line1 );

            geometry = this.lineGeometry2();
            this.line2 = new Line(geometry, material);
            this.line2.name = 'EllipseRadius2';
            this.ellipse.add( this.line2 );

            this.addLinePoint(relative);
        }
        this.addPoint(relative);
        this.updateLine(relative);

        // when drawing of ellipse is completed 
        if(this.count > this.MAX_POINTS) {
            this.count = 0;
            this.ellipse.userData.Selected = false;
            //bounding sphere is set so that line can be selected using mouse
            this.ellipse.geometry.computeBoundingSphere();

            this.engServ.updateSessionStorage();    
            this.aRotation = 0;
        }

    }

    onMouseMove( relative:Vector3, e:any ) {
        if( this.count !== 0 ){
            if (!relative) {
                return;
            }
            this.updateEllipse(relative);   
            this.updateLine(relative);   
            this.updateDimensionsOnDrawing();   
        }
        //if any line is already drawn, check if endpoint or midpoint is enabled to show line point square 
        //if any line alerady exists in group
        if( this.engServ.getSelectedGeometriesGroup().getObjectByName('Line') ) {

            if( this.MS.footer_lineEditor.INTERSECTION || this.MS.footer_lineEditor.NEAREST 
                || this.MS.footer_lineEditor.END_POINT || this.MS.footer_lineEditor.MID_POINT ) {
                
                    this.FS.showLinePointSquare(e, this.ellipse, this.engServ);

            }

        }
    }

    onStop( relative:Vector3 ) {

        //if ellipse is already drawing, only then remove the current ellipse
        if( this.count > 0 ) {
            this.dimensionsShape.remove(this.ellipse);
            //remove last ellipse which is showing with mouse move
            this.engServ.removeFromScene(this.ellipse);
            this.count = 0;
            this.aRotation = 0;
        }   

    }

    ellipseGeometry( xRadius:number, yRadius:number ):BufferGeometry {
        let curve:EllipseCurve = new EllipseCurve(
            this.ellipsePosition.x,  this.ellipsePosition.y,            // ax, aY
            xRadius, yRadius,           // xRadius, yRadius
            0,  2 * Math.PI,  // aStartAngle, aEndAngle
            false,            // aClockwise
            this.aRotation                // aRotation
        );
        const points = curve.getPoints( 50 );
        // geometry
        const geometry:BufferGeometry = new BufferGeometry().setFromPoints( points );
        return geometry;

    }

    // update ellipse
    updateEllipse(relative:Vector3) {

        //if Grid button is enabled in Line Editor page's footer
        //only when drawing circle, grid will work
        if( this.MS.footer_lineEditor.GRID && this.count == 1 ) {

            let lineLength:number;
            let angleBetweenPoints:number;

            lineLength = this.FS.getGridSnapLineLength( this.centerPosition.x, this.centerPosition.y, relative.x, relative.y );
            angleBetweenPoints = this.FS.get_angle_between_points( this.centerPosition.x, this.centerPosition.y, relative.x, relative.y )     
            this.gridSnapPoint = this.FS.get_point_from_angle_distance( this.centerPosition.x, this.centerPosition.y, angleBetweenPoints, lineLength )
    
            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){
                this.gridSnapPoint.copy( this.engServ.linePointSquare.position );
            }

            //get the distance from ellipse center/position to mouse current position
            const radius:number = this.gridSnapPoint.distanceTo( this.centerPosition );
            this.xRadius = radius;

            this.ellipse.userData.radius = {xRadius:this.xRadius,yRadius:this.xRadius};

            const geometry:BufferGeometry = this.ellipseGeometry( radius, radius );
            //update ellipse
            this.ellipse.geometry.attributes.position.array = geometry.attributes.position.array;

        }
        //if no button is enabled, then draw line with exact mouse position
        else {

            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){
                relative.copy( this.engServ.linePointSquare.position );
            }

            //get the distance from ellipse center/position to mouse current position
            let radius:number = relative.distanceTo( this.centerPosition );
            if( radius == 0 ) 
                radius = 0.1;

            let yRadius:number;

            if( this.count == 1) {
                this.xRadius = radius;
                yRadius = radius;  
            }
            else if( this.count == 2) {
                yRadius = radius;  
            }

            this.ellipse.userData.radius = {xRadius:this.xRadius,yRadius:yRadius};
               
            const geometry:BufferGeometry = this.ellipseGeometry( this.xRadius, yRadius );
            //update ellipse
            this.ellipse.geometry.attributes.position.array = geometry.attributes.position.array;

        }

        this.ellipse.geometry.attributes.position.needsUpdate = true;
        this.ellipse.computeLineDistances(); 

    }

    // add point
    addPoint(relative:Vector3){
        //sets the center position of ellipse when click first
        if(this.count == 0) {
            this.centerPosition = relative;
        }
        //sets the aRotation of ellipse
        else if(this.count == 1) {
            let angle = this.FS.get_angle_between_points( this.centerPosition.x, this.centerPosition.y, relative.x, relative.y );
            this.aRotation = this.FS.degrees_to_radians( angle );
        }
        this.count++;  
    }

    updateDimensionsOnDrawing() {
        this.dimensionsShape.update( this.ellipse, this.engServ );
    }   

    lineGeometry1():BufferGeometry { 
        // geometry
        const geometry:BufferGeometry = new BufferGeometry();
        this.positions1 = new Float32Array(6);
        geometry.setAttribute('position', new BufferAttribute(this.positions1, 3));
        return geometry;
    }

    lineGeometry2():BufferGeometry {
        // geometry
        const geometry:BufferGeometry = new BufferGeometry();
        this.positions2 = new Float32Array(6);
        geometry.setAttribute('position', new BufferAttribute(this.positions2, 3));
        return geometry;
    }

    // add point
    addLinePoint(relative:Vector3){
        this.positions1[0] = relative.x;
        this.positions1[1] = relative.y;
        this.positions1[2] = relative.z;
        this.line1.geometry.setDrawRange(0, 2);
        this.line1.geometry.attributes.position.needsUpdate = true;

        this.positions2[0] = relative.x;
        this.positions2[1] = relative.y;
        this.positions2[2] = relative.z;
        this.line2.geometry.setDrawRange(0, 2);
        this.line2.geometry.attributes.position.needsUpdate = true;
    }

    // update line
    updateLine(relative:Vector3) { 

        if( this.count <= 1 ) {
            this.positions1[3] = relative.x;
            this.positions1[4] = relative.y;
            this.positions1[5] = relative.z;   
    
            this.line1.geometry.attributes.position.needsUpdate = true;
            this.line1.computeLineDistances(); 
        }

        this.positions2[3] = relative.x;
        this.positions2[4] = relative.y;
        this.positions2[5] = relative.z;   
    
        this.line2.geometry.attributes.position.needsUpdate = true;
        this.line2.computeLineDistances(); 
        
    }
}
