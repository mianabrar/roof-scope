import { TextureLoader, MeshLambertMaterial, PlaneGeometry, Mesh, Vector3 } from 'three';
import { MessageService } from '../../services/message.service';
import { EngineService } from '../engine.service';
import { Subscription } from 'rxjs';

export class CanvasImage {

    subscription: Subscription;
    private image:Mesh;
    private TOP_IMAGE_URL:any = null;

    constructor( private engServ: EngineService, private MS:MessageService ) {
        // subscribe to component messages
        this.subscription = this.MS.getMessage().subscribe(message => {
        //message from line editor menu
        if (message == "SHOW_CANVAS_IMAGE") {
            this.showImage();
        }
        else if (message == "HIDE_CANVAS_IMAGE") {
            this.hideImage();
        }
      });
    }

    public loadImage() { 

        //no image is browsed, canvas should be blank with no image,
        //load with transparent image,
        //because image is laoded after drawing lines, line get hidden underneath the image
        if( this.MS.imagery.TOP_IMAGE_URL == "assets/images/browseImage.jpg" ) {
            return;
        }

        if( this.TOP_IMAGE_URL == this.MS.imagery.TOP_IMAGE_URL ) {
            return;
        }

        this.removeImage();

        let scope = this;
        let img = new Image();

        img.src = this.MS.imagery.TOP_IMAGE_URL;

        img.onload = function() {
            const img_width:number = img.width;
            const img_height:number = img.height;

            let aspect:number = img_height / img_width;

            let newWidth:number = img_width;

            let newHeight:number = aspect * newWidth;

            scope.loadImageOnCanvas( img, newWidth, newHeight );  
        }  

    }

    private loadImageOnCanvas(img:any, img_width:number, img_height:number) {
        // Create a texture loader so we can load our image file
        var loader = new TextureLoader();

        let imgSrc:any;

        //check if image source is image data or image url
        if( img.src.split(":")[0] == 'data' ) {
            imgSrc = img.src;
        }
        else {
            imgSrc = img.src + "?not-from-cache";
        }
        
        // Load an image file into a custom material
        var material = new MeshLambertMaterial({
            map: loader.load(imgSrc)
        });
    
        // create a plane geometry for the image with a width of 10
        // and a height that preserves the image's aspect ratio
        var geometry = new PlaneGeometry(img_width, img_height);
    
        // combine our image geometry and material into a mesh
        this.image = new Mesh(geometry, material);

        let relativeCenter:Vector3 = this.engServ.get2DPosition(0, 0);

        let x:number = relativeCenter.x + ( img_width / 2 );
        let y:number = relativeCenter.y - ( img_height / 2 );

        // set the position of the image mesh in the x,y,z dimensions
        this.image.position.set( x, y, 0 );
        this.image.name = "Image";
        this.image.renderOrder = 0;
    
        // add the image to the scene
        this.engServ.commonGroup.add( this.image );

        this.MS.sendMessage("ALIGN_IMAGE");

        this.engServ.updateSessionStorage();

        this.TOP_IMAGE_URL = this.MS.imagery.TOP_IMAGE_URL;
    
      }

      private removeImage() {
        //remove Image if already loaded
        try{
            this.engServ.commonGroup.remove( this.image );
            this.TOP_IMAGE_URL = null;
        }
        catch(e){}
      }

      private showImage() {
        try{
            this.image.visible = true;
        }
        catch(e){}
      }

      private hideImage() {
        try{
            this.image.visible = false;
        }
        catch(e){}
    }

}
