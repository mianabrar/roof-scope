import { BufferGeometry, BufferAttribute, LineBasicMaterial, Line, Vector3 } from 'three';
import { Subscription } from 'rxjs';
import { CSS2DObject } from 'three/examples/jsm/renderers/CSS2DRenderer';
import { MessageService } from './../../services/message.service';
import { FunctionService } from './../../services/function.service';
import { EngineService } from '../engine.service';

export class Measure {

    line:any;
    count:number = 0;
    MAX_POINTS:number = 2;
    positions:any;
    scaleLength:number;
    subscription: Subscription;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService ) {
        // subscribe to component messages
        this.subscription = this.MS.getMessage().subscribe(message => {
            
            //input line length changes in measure and align page, so change scale line dimensions if it is already drawn
            if( message == "INPUT_LENGTH_CHANGE" ) {
                //check if scale line exists
                if( this.engServ.commonGroup.getObjectByName('Scale') ) {
                    //remove scale line dimensions from DimensionsGroup
                    let dimensionsGroup = this.engServ.dimensionsGroup; 
                    let dimension = dimensionsGroup.getObjectByName('ScaleDimension');
                    dimensionsGroup.remove( dimension );
                    //attach new dimensions
                    this.attachScaleDimension();
                }                
                
            }    
    
        });
    }

    onMouseDown( relative:Vector3 ) {

        if(this.count > this.MAX_POINTS) {
            this.removeLine();
        }        
       
        // on first click add an extra point
        if( this.count === 0 ){
            const geometry:BufferGeometry = this.lineGeometry();
            this.line = this.lineShape( geometry );
            this.line.name = 'Scale';
            this.line.renderOrder = 1;
            this.engServ.commonGroup.add(this.line);
            this.addPoint(relative);
        }
        this.addPoint(relative);
        
        if(this.count == 3) {
            this.measure();
        }
            
    }

    loadMeasurementLineFromSCD( relative:Vector3 ) {  
       
        if( this.count === 0 ){
            const geometry:BufferGeometry = this.lineGeometry();
            this.line = this.lineShape( geometry );
            this.line.name = 'Scale';
            this.line.renderOrder = 1;
            this.engServ.commonGroup.add(this.line);
            this.addPoint(relative);
        }
        else if( this.count == 1 ) {
            this.addPoint(relative);
        }
        
        if( this.count == 2 ) {
            this.measure();
            this.count = 3;
        }
            
    }


    onMouseMove( relative:Vector3 ) {
        if( this.count !== 0 ){
            if (!relative) {
                return;
            }
            this.updateLine(relative);
        }
    }

    measure() {
        const scaleLength:number = parseFloat(this.line.computeLineDistances().geometry.attributes.lineDistance.array[1].toFixed(2));
        this.MS.input_scaleLength.scaleLength = scaleLength;
        this.attachScaleDimension();
    }

    attachScaleDimension() {

        //remove already assigned last dimensions  
        let dimensionsGroup:any = this.engServ.dimensionsGroup; 
        dimensionsGroup.remove( dimensionsGroup.getObjectByName('ScaleDimension') );

        const shapeArray = this.line.geometry.attributes.position.array;
        const x1:number = shapeArray[0];
        const y1:number = shapeArray[1];
        const x2:number = shapeArray[3];
        const y2:number = shapeArray[4];

        let position:Vector3 = this.FS.get_position_parallel_to_line( x1,y1,x2,y2 );

        let angle:number = this.FS.get_angle_between_points( x1,y1,x2,y2 );

        let decimal:number = this.MS.input_scaleLength.inputLength;
        let text = this.FS.get_formatted_feet(decimal);

        if( Math.abs(angle) < 90 ) {
            angle = angle * -1;
        }
        else {
            angle = 180 - angle;
        }

        let wrapper:any = document.createElement('div');
        let div = document.createElement( 'div' );
    
        div.className = 'dimensions';
        div.textContent = text;
        div.style.fontSize = '10px';
        div.style.color = 'white';
    
        div.style.transform = 'rotate(' + angle + 'deg)';
        
        wrapper.appendChild(div); // Add div to the wrapper that inherits transform from CSS2DObject

        let dimensions:any = new CSS2DObject( wrapper );
        dimensions.position.copy( position );

        dimensions.name = "ScaleDimension";

        
        dimensionsGroup.add( dimensions );

        this.engServ.updateSessionStorage();
    }

    lineGeometry():BufferGeometry {
        // geometry
        let geometry = new BufferGeometry();   
        this.positions = new Float32Array(this.MAX_POINTS * 3);
        geometry.addAttribute('position', new BufferAttribute(this.positions, 3));
        return geometry;
    }

    lineShape( geometry:BufferGeometry ):Line {
        // material
        let material = new LineBasicMaterial({
            color: this.MS.color.LINE
        }); 
        // line
        const line = new Line(geometry, material);
        return line;
    }

    // update line
    updateLine( relative:Vector3 ) {
        this.positions[this.count * 3 - 3] = relative.x;
        this.positions[this.count * 3 - 2] = relative.y + 0.0;
        this.positions[this.count * 3 - 1] = relative.z;
        this.line.geometry.attributes.position.needsUpdate = true;
    }

    //remove line
    removeLine() {
        this.count = 0;
        //remove Measure line and dimensions if already drawn
        try{
            //remove scale line
            const scale = this.engServ.commonGroup.getObjectByName('Scale');
            this.engServ.commonGroup.remove(scale);

            //remove scale line dimensions from DimensionsGroup
            let dimensionsGroup = this.engServ.dimensionsGroup; 
            let dimensions = dimensionsGroup.getObjectByName('ScaleDimension');
            dimensionsGroup.remove( dimensions );
        }
        catch(e){
            console.log(e)
        }
    }
 
    // add point
    addPoint( relative:Vector3 ){ 
        this.positions[this.count * 3 + 0] = relative.x;
        this.positions[this.count * 3 + 1] = relative.y + 0.0;
        this.positions[this.count * 3 + 2] = relative.z;
        this.count++;
        this.line.geometry.setDrawRange(0, this.count);
        this.updateLine(relative);
        
    }

    getAngleBetweenPoints() {
        const camera:any = this.engServ.camera;
        const cameraRotation:number = this.FS.radians_to_degrees(camera.rotation.z);
 
        // angle in degrees
        let angleDeg:number = Math.atan2(this.positions[4] - this.positions[1], this.positions[3] - this.positions[0]) * 180 / Math.PI;

        if( Math.sign(angleDeg) == -1 ) {
            return Math.abs( angleDeg + cameraRotation );
        }
        else if( Math.sign(angleDeg) == 1 ) {
            return Math.abs( angleDeg - 360 - cameraRotation );
        }
        else if( Math.sign(angleDeg) == 0 ) {
            return 0 - cameraRotation;
        }    
      
    }

    show() {
        if( this.line ) {
            this.line.visible = true;
            this.attachScaleDimension();
        }
    }

    hide() {
        if( this.line ) {
            this.line.visible = false;
            //remove scale line dimensions from DimensionsGroup
            let dimensionsGroup = this.engServ.dimensionsGroup; 
            let dimension = dimensionsGroup.getObjectByName('ScaleDimension');
            dimensionsGroup.remove( dimension );
        }
            
    }

}
