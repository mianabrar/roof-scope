import { Vector3, BufferGeometry, ArcCurve } from 'three';
import { EngineService } from '../../engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from 'src/app/services/function.service';

export class ModifyStretchArc {

    object:any;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService ) {

    }

    // update Arc
    update(relative:Vector3) {

        let edge1:Vector3 = this.object.getObjectByName('LineEdge1').position;
        let edge2:Vector3 = this.object.getObjectByName('LineEdge2').position;
        let edge3:Vector3 = this.object.getObjectByName('LineEdge3').position;

        //if line end point or mid point is enabled and line point square is visible
        //means mouse is over end point or midpoint
        //then add point will use line point square position
        if( this.engServ.linePointSquare.visible ){

            //if edge1 is dragging
            if( Math.floor( relative.x) == Math.floor( edge1.x ) && Math.floor( relative.y) == Math.floor( edge1.y) ) {
                edge1.copy( this.engServ.linePointSquare.position );
            }
            //if edge2 is dragging
            else if( Math.floor( relative.x) == Math.floor( edge2.x ) && Math.floor( relative.y) == Math.floor( edge2.y) ) {
                edge2.copy( this.engServ.linePointSquare.position );
            }
            //if edge3 is dragging
            else if( Math.floor( relative.x) == Math.floor( edge3.x ) && Math.floor( relative.y) == Math.floor( edge3.y) ) {
                edge3.copy( this.engServ.linePointSquare.position );
            }
            
        }

        const geometry:BufferGeometry = this.arcGeometry( edge1, edge2, edge3 );
        //update arc
        this.object.geometry.attributes.position.array = geometry.attributes.position.array;
        this.object.geometry.attributes.position.needsUpdate = true;

        //bounding sphere is set so that line can be selected using mouse
        this.object.geometry.computeBoundingSphere();
    }

    private arcGeometry( edge1:Vector3,  edge2:Vector3,  edge3:Vector3 ):BufferGeometry {

        //get circle center from 3 points
        const circleCenter:Vector3 = this.FS.get_circle_center( edge1.x, edge1.y, edge2.x, edge2.y, edge3.x, edge3.y );
        //get circle radius
        const aRadius:number = this.FS.get_distance_between_points( circleCenter.x, circleCenter.y, edge3.x, edge3.y );
        //get start angle of the point where line starts
        const aStartAngle:number = this.FS.get_circle_point_angle( circleCenter.x, circleCenter.y, edge3.x, edge3.y );
        //get end angle of the point where line ends
        const aEndAngle:number = this.FS.get_circle_point_angle( circleCenter.x, circleCenter.y, edge1.x, edge1.y );
        //check relative is on what side on line is
        const aClockwise:boolean = this.FS.if_point_is_above_line( edge1.x, edge1.y, edge3.x, edge3.y, edge2.x, edge2.y );

        const curve:ArcCurve = new ArcCurve(
            circleCenter.x, circleCenter.y,        // ax, aY
            aRadius,           // aRadius
            aStartAngle,  aEndAngle,  // aStartAngle, aEndAngle
            aClockwise            // aClockwise
        );
        const points = curve.getPoints( 50 );
  
        // geometry
        const geometry:BufferGeometry = new BufferGeometry().setFromPoints( points );     
        return geometry;
    }
}
