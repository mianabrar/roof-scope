import { Vector3, BufferGeometry, EllipseCurve } from 'three';
import { EngineService } from '../../engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from 'src/app/services/function.service';

export class ModifyStretchCircle {

    object:any;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService ) {

    }

    // update Circle
    update(relative:Vector3) {

        let edge1:Vector3 = this.object.getObjectByName('LineEdge1').position;
        let edge2:Vector3 = this.object.getObjectByName('LineEdge2').position;

        //if line end point or mid point is enabled and line point square is visible
        //means mouse is over end point or midpoint
        //then add point will use line point square position
        if( this.engServ.linePointSquare.visible ){

            //if edge1 is dragging
            if( Math.floor( relative.x) == Math.floor( edge1.x ) && Math.floor( relative.y) == Math.floor( edge1.y) ) {
                edge1.copy( this.engServ.linePointSquare.position );
            }
            //if edge2 is dragging
            else if( Math.floor( relative.x) == Math.floor( edge2.x ) && Math.floor( relative.y) == Math.floor( edge2.y) ) {
                edge2.copy( this.engServ.linePointSquare.position );
            }
            
        }

        //get the distance from circle center/position edge to circle edge
        let radius:number = edge1.distanceTo( edge2 );
        if( radius == 0 ) 
            radius = 0.1;

        const geometry:BufferGeometry = this.circleGeometry( radius, edge2 );

        //update circle
        this.object.geometry.attributes.position.array = geometry.attributes.position.array;
        this.object.geometry.attributes.position.needsUpdate = true;

        //bounding sphere is set so that line can be selected using mouse
        this.object.geometry.computeBoundingSphere();
            
    }

    private circleGeometry( radius:number, position:Vector3 ):BufferGeometry {    
        let curve:EllipseCurve = new EllipseCurve(
            position.x,  position.y,            // ax, aY
            radius, radius,           // xRadius, yRadius
            0,  2 * Math.PI,  // aStartAngle, aEndAngle
            false,            // aClockwise
            0                // aRotation
        );   
        const points = curve.getPoints( 50 );
        // geometry
        const geometry:BufferGeometry = new BufferGeometry().setFromPoints( points );  
        return geometry;
    }

    // update line
    updateLine() {
        this.object.geometry.attributes.position.array

        let line:any = this.object.getObjectByName("CircleRadius");
        var linePosition = line.geometry.attributes.position.array;

        this.object.geometry.attributes.position.needsUpdate = true;
    }

}
